<?php

$rnd = rand(1,9);
$base_uri =  get_template_directory_uri();
$hero = "url($base_uri/resources/agep_banner_$rnd.jpg)";
$lang = function_exists("pll_current_language") ? (pll_current_language() != "en") : true; //true is fr_FR and false is en_GB

?>

<main id="content" role="main">
  <!-- Hero Section -->
  <div class="gradient-overlay-half-dark-v1 bg-img-hero" style="background-image: <?php echo $hero ?>;">
    <div class="d-lg-flex align-items-lg-center flex-lg-column">
      <div class="container space-3 space-4-top--lg">
        <!-- Title -->
        <div class="w-md-50">
          <h1 class="display-4 font-size-48--md-down text-white">AGEPoly</h1>
          <p class="lead text-white"><?php echo $lang ? "Association Générale des Étudiants de l'EPFL" : "General Student's Association of EPFL"; ?></p>
        </div>
        <!-- End Title -->
      </div>
    </div>

    <div class="container space-1-bottom">
      <!-- Fancybox -->
      <div class="d-inline-block">
        <a class="js-fancybox u-media-player media align-items-center text-white" href="javascript:;"
           data-src="//www.youtube.com/watch?v=8avbLNgtebk&feature=youtu.be"
           data-speed="700"
           data-animate-in="zoomIn"
           data-animate-out="zoomOut"
           data-caption="<?php echo $lang ? "L'AGEPoly : qu'est-ce que c'est ?" : "What is AGEPoly?"; ?>">
          <span class="u-media-player__icon mr-3">
            <span class="fa fa-play u-media-player__icon-inner"></span>
          </span>
          <span class="media-body">
            <small class="d-block text-uppercase">Video</small>
            <?php echo $lang ? "L'AGEPoly : qu'est-ce que c'est ?" : "What is AGEPoly?"; ?>
          </span>
        </a>
      </div>
      <!-- End Fancybox -->
    </div>
  </div>
  <!-- End Hero Section -->

<!-- Features Section -->
<div class="container text-center space-2">
  <div class="row">
   <?php

  $block_menu = wp_get_nav_menu_object( get_nav_menu_locations()['block-menu'] );

  if ( $block_menu && ! is_wp_error($block_menu) && !isset($block_menu_items) )
    $block_menu_items = wp_get_nav_menu_items( $block_menu->term_id, array( 'update_post_term_cache' => false ) );

    // Set up the $menu_item variables
      _wp_menu_item_classes_by_context( $block_menu_items );

      $sorted_block_items = $menu_items_with_children = array();
      foreach ( (array) $block_menu_items as $menu_item ) {
          $sorted_block_items[ $menu_item->menu_order ] = $menu_item;
          if ( $menu_item->menu_item_parent )
              $menu_items_with_children[ $menu_item->menu_item_parent ] = true;
      }

      // Add the menu-item-has-children class where applicable
      if ( $menu_items_with_children ) {
          foreach ( $sorted_block_items as &$menu_item ) {
              if ( isset( $menu_items_with_children[ $menu_item->ID ] ) )
                  $menu_item->classes[] = 'menu-item-has-children';
          }
      }

   ?>
   
   <?php

				$count = 0; 
              foreach ($sorted_block_items as $item) {
				  
                if ($item->menu_item_parent == 0) {
                  ?>
					<div class="col-sm-6 col-lg-4 offset-sm-3 offset-lg-0 order-lg-2 mb-7 mb-sm-0">
						<!-- Icon Block -->
						<div class="<?php echo $count%2 == 1 ? "bg-primary" : ""  ?> text-center rounded py-9 p-5">
							<h2 class="h4 <?php echo $count%2 == 1 ? "text-white" : "text-dark"  ?>"><?php echo $item->title?></h2>
							<p class="<?php echo $count%2 == 1 ? "text-white" : "text-dark"  ?>"><?php echo $item->description ?></p>
							<a class="<?php echo $count%2 == 1 ? "text-white" : "text-dark"  ?>" href="<?php echo $item->url  ?>">
								<?php echo $lang ? "Plus d'infos" : "More info"; ?>
								<span class="fa fa-angle-right align-middle ml-2"></span>
							</a>
						</div>
						<!-- End Icon Block -->
					</div>
                <?php
				$count++;
                }
              }

               ?>

  </div>
</div>
<!-- End Features Section -->

<!-- Blog-->

<?php

if (have_posts()) { ?>
  <div class="container text-center">
  <h3 class="space-1-bottom">Blog</h3>
  <div class="card-deck d-block d-lg-flex">
  <?php
  include get_theme_root()."/".get_template().'/html/thumb_blog.php';

  if (have_posts()) {
    include get_theme_root()."/".get_template().'/html/thumb_blog.php';
  }
?>
  </div>
</div> <?php
}

 ?>

<!-- End Blog -->

<!-- Instagram -->

<div class="container text-center space-1-bottom">
  <h3 class="space-1-bottom">Instagram</h3>
  <div id="instaFeed" class="js-instagram row mx-gutters-2 container"
  data-user-id="1818204499"
  data-client-id="cc9ed44607064cdebe2e071bc86ea9da"
  data-token="1818204499.cc9ed44.4a1a13934ed44a4eb9ef3f8ac22437bf"
  data-limit="4"
  data-template='<div class="col-md-3 cust_centered"><a href="{{link}}" target="_blank"><img class="img-fluid w-100 rounded" src="{{image}}" /></a></div>'>
  </div>
</Sdiv>

</main>
