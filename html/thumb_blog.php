
  <!-- Blog Grid -->

  <?php
    if (have_posts()) {
      the_post();
    }
   ?>

  <article class="card mb-5">
  <div class="card-body p-5">
    <small class="d-block-2"><?php the_date(); ?></small>
    <h2 class="h5">
    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
    </h2>
    <p class="mb-0"><?php the_excerpt(); ?></p>
  </div>
  <div class="card-footer bg-gray-100 py-4 px-5">
    <div class="media">
    <img class="u-sm-avatar rounded-circle mr-3" src="<?php echo get_avatar_url($post); ?>">
    <div class="media-body">
      <h3 class="d-inline-block mb-0">
      <a class="d-block font-size-13" href="#"><?php the_author(); ?></a>
      </h3>
    </div>
    </div>
  </div>
  </article>
  <!-- End Blog Grid -->
