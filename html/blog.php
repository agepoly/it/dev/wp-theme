<?php the_post(); ?>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/blog.css">

<main id="content" role="main">

<!-- Text -->
<div class="container space-2">
  <h5 class="author"><?php the_author(); ?></h2>
  <h6 align="center"><?php the_date(); ?></h3>
  <p><?php the_content(); ?></p>
</div>


</main>
