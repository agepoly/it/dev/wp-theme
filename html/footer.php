<?php

$lang = function_exists("pll_current_language") ? (pll_current_language() != "en") : true; //true is fr_FR and false is en_GB

?>



  <footer class="bg-dark">
  <div class="container space-2">
    <div class="row justify-content-md-between">
	 <?php

  $footer_menu = wp_get_nav_menu_object( get_nav_menu_locations()['footer-menu'] );

  if ( $footer_menu && ! is_wp_error($footer_menu) && !isset($footer_menu_items) )
    $footer_menu_items = wp_get_nav_menu_items( $footer_menu->term_id, array( 'update_post_term_cache' => false ) );

    // Set up the $menu_item variables
      _wp_menu_item_classes_by_context( $footer_menu_items );

      $sorted_footer_items = $menu_items_with_children = array();
      foreach ( (array) $footer_menu_items as $menu_item ) {
          $sorted_footer_items[ $menu_item->menu_order ] = $menu_item;
          if ( $menu_item->menu_item_parent )
              $menu_items_with_children[ $menu_item->menu_item_parent ] = true;
      }

      // Add the menu-item-has-children class where applicable
      if ( $menu_items_with_children ) {
          foreach ( $sorted_footer_items as &$menu_item ) {
              if ( isset( $menu_items_with_children[ $menu_item->ID ] ) )
                  $menu_item->classes[] = 'menu-item-has-children';
          }
      }

   ?>
   
   <?php

              foreach ($sorted_footer_items as $item) {
                if ($item->menu_item_parent == 0) {
                  ?>
                    <div class="col-6 col-md-3 col-lg-2 order-lg-3 mb-7 mb-lg-0">
						<h3 class="h6 text-white mb-3"><?php echo $item->title; ?></h3>

							<!-- List Group -->
							<div class="list-group list-group-flush list-group-transparent">
				  
                <?php
                  foreach ($sorted_footer_items as $child) {
                    if ($child->menu_item_parent == $item->ID) {
				?>
				  <a class="list-group-item list-group-item-action" href="<?php echo $child->url; ?>"><?php echo $child->title; ?></a>
                <?php
					
                    }
                  }
					
				?>
							</div>
							<!-- End List Group -->
					</div>
				
                <?php
                }
              }

               ?>



      <div class="col-lg-4 order-lg-1 d-flex align-items-start flex-column">

        <!-- Language -->
        <div class="btn-group d-block position-relative mb-5 mb-lg-auto">
            <?php if (function_exists("pll_the_languages")) {
              $langs = pll_the_languages(array("raw" => 1));
              foreach ($langs as $lang) {
                if (!($lang["current_lang"])) { ?>
                  <a id="footerLanguageInvoker" class="btn-text-secondary d-flex align-items-center u-unfold--language-btn rounded py-2 px-3" href="<?php echo $lang["url"]; ?>" role="button">
                  <img class="max-width-3 mr-2" src="<?php echo $lang["flag"]; ?>" alt="Lang Flag">
                  <span class="font-size-14"><?php echo $lang["name"]; ?></span>
                <?php }
              }
            } ?>
          </a>
        </div>
        <!-- End Language -->
		
		        <!-- Logo -->
    <div class="d-inline-block">
        <a class="d-inline-block" href="<?php echo get_home_url(); ?>" aria-label="Space">
          <img src="<?php echo get_template_directory_uri(); ?>/resources/Horizontal_-_AI_-_AGEPoly.svg" alt="Logo" style="width: 100%;">
        </a>
    </div>
        <!-- End Logo -->


        <p class="small text-muted"><?php echo $lang ? "Tous droits réservés" : "All rights reserved"; ?> &copy; AGEPoly 2019.</p>
      </div>
    </div>
  </div>
</footer>
<!-- ========== END FOOTER ========== -->

<!-- ========== SECONDARY CONTENTS ========== -->

<!-- ========== END SECONDARY CONTENTS ========== -->

<!-- Go to Top -->
<a class="js-go-to u-go-to" href="javascript:;"
  data-position='{"bottom": 15, "right": 15 }'
  data-type="fixed"
  data-offset-top="400"
  data-compensation="#header"
  data-show-effect="slideInUp"
  data-hide-effect="slideOutDown">
  <span class="fa fa-arrow-up u-go-to__inner"></span>
</a>
<!-- End Go to Top -->

<!-- JS Global Compulsory -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/jquery-migrate/dist/jquery-migrate.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/popper.js/dist/umd/popper.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/bootstrap/bootstrap.min.js"></script>

<!-- JS Implementing Plugins -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/custombox/dist/custombox.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/custombox/dist/custombox.legacy.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/slick-carousel/slick/slick.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/fancybox/jquery.fancybox.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/player.js/dist/player.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/instafeed.js/instafeed.min.js"></script>

<!-- JS Space -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/hs.core.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/components/hs.header.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/components/hs.unfold.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/components/hs.validation.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/helpers/hs.focus-state.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/components/hs.malihu-scrollbar.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/components/hs.modal-window.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/components/hs.show-animation.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/components/hs.slick-carousel.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/components/hs.video-player.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/components/hs.fancybox.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/components/hs.go-to.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/components/hs.instagram.js"></script>

<!-- JS Plugins Init. -->
<script>
  $(window).on('load', function () {
    // initialization of HSMegaMenu component
    $('.js-mega-menu').HSMegaMenu({
      event: 'hover',
      pageContainer: $('.container'),
      breakpoint: 991,
      hideTimeOut: 0
    });
  });

  $(document).on('ready', function () {
    // initialization of header
    $.HSCore.components.HSHeader.init($('#header'));

    // initialization of unfold component
    $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
      afterOpen: function () {
        if (!$('body').hasClass('IE11')) {
          $(this).find('input[type="search"]').focus();
        }
      }
    });

    // initialization of form validation
    $.HSCore.components.HSValidation.init('.js-validate', {
      rules: {
        confirmPassword: {
          equalTo: '#password'
        }
      }
    });

    // initialization of forms
    $.HSCore.helpers.HSFocusState.init();

    // initialization of malihu scrollbar
    $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

    // initialization of autonomous popups
    $.HSCore.components.HSModalWindow.init('[data-modal-target]', '.js-signup-modal', {
      autonomous: true
    });

    // initialization of show animations
    $.HSCore.components.HSShowAnimation.init('.js-animation-link');

    // initialization of slick carousel
    $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

    // initialization of fancybox
    $.HSCore.components.HSFancyBox.init('.js-fancybox');

    // initialization of video player
    $.HSCore.components.HSVideoPlayer.init('.js-inline-video-player');

    // initialization of go to
    $.HSCore.components.HSGoTo.init('.js-go-to');
  });
</script>
<script>
  $(document).on('ready', function () {
  // initialization of instagram api
  $.HSCore.components.HSInstagram.init('#instaFeed', {
    resolution: 'standard_resolution',
    after: function () {
    // initialization of masonry.js
    var $grid = $('.js-instagram').masonry({
      percentPosition: true
    });

    // initialization of images loaded
    $grid.imagesLoaded().progress(function () {
      $grid.masonry();
    });
    }
  });
  });
</script>
</body>
</html>
