<html lang="en">
<head>
  <!-- Title -->
  <title>AGEPoly</title>

  <!-- Required Meta Tags Always Come First -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Favicon -->
  <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/resources/ico-agepoly-vertical.png">

  <!-- Google Fonts -->
  <link href="//fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">

  <!-- CSS Global Compulsory -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/vendor/bootstrap/css/bootstrap.css">

  <!-- CSS Implementing Plugins -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/vendor/font-awesome/css/all.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/vendor/hs-megamenu/src/hs.megamenu.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/vendor/custombox/dist/custombox.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/vendor/animate.css/animate.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/vendor/slick-carousel/slick/slick.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/vendor/fancybox/jquery.fancybox.css">

  <!-- CSS Space Template -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/theme.min.css">
</head>

<body>
  <!-- Skippy -->
  <a id="skippy" class="sr-only sr-only-focusable u-skippy" href="#content">
    <div class="container">
      <span class="u-skiplink-text">Skip to main content</span>
    </div>
  </a>
  <!-- End Skippy -->


  <?php

  $menu = wp_get_nav_menu_object( get_nav_menu_locations()['header-menu'] );

  if ( $menu && ! is_wp_error($menu) && !isset($menu_items) )
    $menu_items = wp_get_nav_menu_items( $menu->term_id, array( 'update_post_term_cache' => false ) );

    // Set up the $menu_item variables
      _wp_menu_item_classes_by_context( $menu_items );

      $sorted_menu_items = $menu_items_with_children = array();
      foreach ( (array) $menu_items as $menu_item ) {
          $sorted_menu_items[ $menu_item->menu_order ] = $menu_item;
          if ( $menu_item->menu_item_parent )
              $menu_items_with_children[ $menu_item->menu_item_parent ] = true;
      }

      // Add the menu-item-has-children class where applicable
      if ( $menu_items_with_children ) {
          foreach ( $sorted_menu_items as &$menu_item ) {
              if ( isset( $menu_items_with_children[ $menu_item->ID ] ) )
                  $menu_item->classes[] = 'menu-item-has-children';
          }
      }

  //if (in_array('menu-item-has-children',$menu_items[0]->classes)) echo implode($menu_items[0]->classes);
    //var_dump($menu_items[1]);

   ?>

  <!-- ========== HEADER ========== -->
  <header id="header" class="u-header">
    <div class="u-header__section">
      <div id="logoAndNav" class="container">
        <!-- Nav -->
        <nav class="js-mega-menu navbar navbar-expand-lg u-header__navbar">
          <!-- Logo -->
          <div class="u-header__navbar-brand-wrapper">
            <a class="navbar-brand u-header__navbar-brand" href="
			<?php echo get_home_url(); if (function_exists("pll_current_language")) {if (pll_current_language() == "en") echo "?lang=en";} ?>" aria-label="Space">
              <img class="u-header__navbar-brand-default" src="<?php echo get_template_directory_uri(); ?>/resources/Horizontal_-_AI_-_AGEPoly.svg" alt="Logo">
              <img class="u-header__navbar-brand-mobile" src="<?php echo get_template_directory_uri(); ?>/resources/Vertical_-_AI_-_AGEPoly.svg" alt="Logo">
            </a>
          </div>
          <!-- End Logo -->

          <!-- Responsive Toggle Button -->
          <button type="button" class="navbar-toggler btn u-hamburger u-header__hamburger"
                  aria-label="Toggle navigation"
                  aria-expanded="false"
                  aria-controls="navBar"
                  data-toggle="collapse"
                  data-target="#navBar">
            <span class="d-none d-sm-inline-block">Menu</span>
            <span id="hamburgerTrigger" class="u-hamburger__box ml-3">
              <span class="u-hamburger__inner"></span>
            </span>
          </button>
          <!-- End Responsive Toggle Button -->

          <!-- Navigation -->
          <div id="navBar" class="collapse navbar-collapse u-header__navbar-collapse py-0">
            <ul class="navbar-nav u-header__navbar-nav">
              <!-- AGEPoly -->

              <?php

              foreach ($sorted_menu_items as $item) {
                if ($item->menu_item_parent == 0) {
                  if (in_array('menu-item-has-children',$item->classes)) {?>
                  <li class="nav-item hs-has-sub-menu u-header__nav-item"
                      data-event="hover"
                      data-animation-in="fadeInUp"
                      data-animation-out="fadeOut">
                    <a id="shopMegaMenu" class="nav-link u-header__nav-link" href="<?php echo $item->url;?>"
                       aria-haspopup="true"
                       aria-expanded="false"
                       aria-labelledby="shopSubMenu">
                      <?php echo $item->title; ?>
                      <span class="fa fa-angle-down u-header__nav-link-icon"></span>
                    </a>
                    <ul id="shopSubMenu" class="list-inline hs-sub-menu u-header__sub-menu mb-0" style="min-width: 220px;"
                        aria-labelledby="shopMegaMenu">
                    <?php
                  foreach ($sorted_menu_items as $child) {
                    if ($child->menu_item_parent == $item->ID) {?>
                      <li class="dropdown-item u-header__sub-menu-list-item py-0">
                        <a class="nav-link u-header__sub-menu-nav-link" href="<?php echo $child->url;?>"><?php echo $child->title;?></a>
                      </li>
                    <?php
                    }
                  }
                  echo "</ul>";
                } else {?>
                  <li class="nav-item u-header__nav-item"
                      data-event="hover"
                      data-animation-in="fadeInUp"
                      data-animation-out="fadeOut">
                    <a id="shopMegaMenu" class="nav-link u-header__nav-link" href="<?php echo $item->url;?>">
                      <?php echo $item->title; ?>
                    </a>
                  </li>
                  <?php
                }
              }
              }

               ?>

              <!-- Button -->
              <li class="nav-item u-header__nav-item-btn">
                <a class="btn btn-sm btn-primary" href="https://truffe2.agepoly.ch/" role="button">
                  <span class="fa fa-user-circle mr-1"></span>
                  Intranet
                </a>
              </li>
              <!-- End Button -->

			  <!-- Social -->
			  <li class="nav-item u-header__navbar-icon u-header__navbar-v-divider">
				<a type="button" class="btn btn-xs btn-icon btn-facebook rounded" href="https://www.facebook.com/AGEPoly/">
				  <span class="fab fa-facebook-f btn-icon__inner"></span>
				</a>
				<a type="button" class="btn btn-xs btn-icon btn-twitter rounded" href="https://twitter.com/agepoly?lang=fr">
				  <span class="fab fa-twitter btn-icon__inner"></span>
				</a>
				<a type="button" class="btn btn-xs btn-icon btn-instagram rounded" href="https://www.instagram.com/agepoly/">
				  <span class="fab fa-instagram btn-icon__inner"></span>
				</a>
			  </li>
			<?php if (function_exists("pll_the_languages")) pll_the_languages(array("show_flags" => 1, "show_names" => 0, "hide_current" => 1)); ?>

            </ul>
          </div>
          <!-- End Navigation -->
        </nav>
        <!-- End Nav -->
      </div>
    </div>
  </header>
