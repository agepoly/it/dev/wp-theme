<!-- Team Section -->
<div class="container">
  <div class="row mb-9 text-center">
  <div class="col-6 col-lg-3 mb-7 mb-lg-0 space-1 cust_centered">
    <!-- Team -->
    <div class="text-center">
    <img class="u-lg-avatar shadow-lg rounded-circle mx-auto mb-4" src="<?php echo get_template_directory_uri(); ?>/resources/avatar-diane.jpg" alt="Image Description">
    <h4 class="h5">Diane Bernard-Bruls</h4>
    <span class="d-block text-primary">Présidente</span>
    <hr>
    <a class="text-secondary font-size-14" href="#">presidente@agepoly.ch</a>
    </div>
    <!-- End Team -->
  </div>

  <div class="col-6 col-lg-3 mb-7 mb-lg-0 space-1 cust_centered">
    <!-- Team -->
    <div class="text-center">
    <img class="u-lg-avatar shadow-lg rounded-circle mx-auto mb-4" src="<?php echo get_template_directory_uri(); ?>/resources/avatar-leo.jpg" alt="Image Description">
    <h4 class="h5">Léo Meynent</h4>
    <span class="d-block text-primary">Vice-Président</span>
    <hr>
    <a class="text-secondary font-size-14" href="#">vice-president@agepoly.ch</a>
    </div>
    <!-- End Team -->
  </div>

  <div class="col-6 col-lg-3 space-1 cust_centered">
    <!-- Team -->
    <div class="text-center">Responsable Communication

    <img class="u-lg-avatar shadow-lg rounded-circle mx-auto mb-4" src="<?php echo get_template_directory_uri(); ?>/resources/avatar-fouco.jpg" alt="Image Description">
    <h4 class="h5">Foucauld Tabourin</h4>
    <span class="d-block text-primary">Administrateur</span>
    <hr>
    <a class="text-secondary font-size-14" href="#">administration@agepoly.ch</a>
    </div>
    <!-- End Team -->
  </div>

  <div class="col-6 col-lg-3 space-1 cust_centered">
    <!-- Team -->
    <div class="text-center">
    <img class="u-lg-avatar shadow-lg rounded-circle mx-auto mb-4" src="<?php echo get_template_directory_uri(); ?>/resources/avatar-jon.jpg" alt="Image Description">
    <h4 class="h5">Jonathan Collaud</h4>
    <span class="d-block text-primary">Secrétaire Général</span>
    <hr>
    <a class="text-secondary font-size-14" href="#">secretaire.general@agepoly.ch</a>
    </div>
    <!-- End Team -->
  </div>

  <div class="col-6 col-lg-3 space-1 cust_centered">
    <!-- Team -->
    <div class="text-center">
    <img class="u-lg-avatar shadow-lg rounded-circle mx-auto mb-4" src="<?php echo get_template_directory_uri(); ?>/resources/avatar-nicolo.jpg" alt="Image Description">
    <h4 class="h5">Nicolò Ferrari</h4>
    <span class="d-block text-primary">Responsable AGEPolytique</span>
    <hr>
    <a class="text-secondary font-size-14" href="#">agepolytique@agepoly.ch</a>
    </div>
    <!-- End Team -->
  </div>

  <div class="col-6 col-lg-3 space-1 cust_centered">
    <!-- Team -->
    <div class="text-center">
    <img class="u-lg-avatar shadow-lg rounded-circle mx-auto mb-4" src="<?php echo get_template_directory_uri(); ?>/resources/avatar-pj.jpg" alt="Image Description">
    <h4 class="h5">Pierre-Jean Martin</h4>
    <span class="d-block text-primary">Responsable Animation</span>
    <hr>
    <a class="text-secondary font-size-14" href="#">animation@agepoly.ch</a>
    </div>
    <!-- End Team -->
  </div>

  <div class="col-6 col-lg-3 space-1 cust_centered">
    <!-- Team -->
    <div class="text-center">
    <img class="u-lg-avatar shadow-lg rounded-circle mx-auto mb-4" src="<?php echo get_template_directory_uri(); ?>/resources/avatar-stache.jpg" alt="Image Description">
    <h4 class="h5">Hugo Hueber</h4>
    <span class="d-block text-primary">Responsable Services</span>
    <hr>
    <a class="text-secondary font-size-14" href="#">services@agepoly.ch</a>
    </div>
    <!-- End Team -->
  </div>

  <div class="col-6 col-lg-3 space-1 cust_centered">
    <!-- Team -->
    <div class="text-center">
    <img class="u-lg-avatar shadow-lg rounded-circle mx-auto mb-4" src="<?php echo get_template_directory_uri(); ?>/resources/avatar-marcel.jpg" alt="Image Description">
    <h4 class="h5">Nicolas Lesimple</h4>
    <span class="d-block text-primary">Responsable Sports</span>
    <hr>
    <a class="text-secondary font-size-14" href="#">sports@agepoly.ch</a>
    </div>
    <!-- End Team -->
  </div>

  <div class="col-6 col-lg-3 space-1 cust_centered">
    <!-- Team -->
    <div class="text-center">
    <img class="u-lg-avatar shadow-lg rounded-circle mx-auto mb-4" src="<?php echo get_template_directory_uri(); ?>/resources/avatar-juju.jpg" alt="Image Description">
    <h4 class="h5">Juliette Meurgey</h4>
    <span class="d-block text-primary">Responsable Logistique</span>
    <hr>
    <a class="text-secondary font-size-14" href="#">logistique@agepoly.ch</a>
    </div>
    <!-- End Team -->
  </div>

  <div class="col-6 col-lg-3 space-1 cust_centered">
    <!-- Team -->
    <div class="text-center">
    <img class="u-lg-avatar shadow-lg rounded-circle mx-auto mb-4" src="<?php echo get_template_directory_uri(); ?>/resources/avatar-david.jpg" alt="Image Description">
    <h4 class="h5">David Cleres</h4>
    <span class="d-block text-primary">Responsable Relations Externes</span>
    <hr>
    <a class="text-secondary font-size-14" href="#">relex@agepoly.ch</a>
    </div>
    <!-- End Team -->
  </div>

  <div class="col-6 col-lg-3 space-1 cust_centered">
    <!-- Team -->
    <div class="text-center">
    <img class="u-lg-avatar shadow-lg rounded-circle mx-auto mb-4" src="<?php echo get_template_directory_uri(); ?>/resources/avatar-delph.jpg" alt="Image Description">
    <h4 class="h5">Delphine Zihlmann</h4>
    <span class="d-block text-primary">Responsable Commissions</span>
    <hr>
    <a class="text-secondary font-size-14" href="#">commissions@agepoly.ch</a>
    </div>
    <!-- End Team -->
  </div>

  <div class="col-6 col-lg-3 space-1 cust_centered">
    <!-- Team -->
    <div class="text-center">
    <img class="u-lg-avatar shadow-lg rounded-circle mx-auto mb-4" src="<?php echo get_template_directory_uri(); ?>/resources/avatar-roos.jpg" alt="Image Description">
    <h4 class="h5">Roosembert Palacios</h4>
    <span class="d-block text-primary">Responsable Informatique</span>
    <hr>
    <a class="text-secondary font-size-14" href="#">informatique@agepoly.ch</a>
    </div>
    <!-- End Team -->
  </div>

  <div class="col-6 col-lg-3 space-1 cust_centered">
    <!-- Team -->
    <div class="text-center">
    <img class="u-lg-avatar shadow-lg rounded-circle mx-auto mb-4" src="<?php echo get_template_directory_uri(); ?>/resources/avatar-loan.jpg" alt="Image Description">
    <h4 class="h5">Loan Dao</h4>
    <span class="d-block text-primary">Responsable Communication</span>
    <hr>
    <a class="text-secondary font-size-14" href="#">communication@agepoly.ch</a>
    </div>
    <!-- End Team -->
  </div>
  </div>
</div>
<!-- End Team Section -->

<h1 class="text-center">Collaborateurs</h1>

<!-- Team Section -->
<div class="container">
  <div class="row mb-9 text-center">
  <div class="col-6 col-lg-3 mb-7 mb-lg-0 space-1 cust_centered">
    <!-- Team -->
    <div class="text-center">
    <img class="u-lg-avatar shadow-lg rounded-circle mx-auto mb-4" src="<?php echo get_template_directory_uri(); ?>/assets/img/100x100/img1.jpg" alt="Image Description">
    <h4 class="h5">Marianne Jenny Nguyen</h4>
    <span class="d-block text-primary">Secrétaire</span>
    <hr>
    <a class="text-secondary font-size-14" href="#">contact@agepoly.ch</a>
    </div>
    <!-- End Team -->
  </div>

  <div class="col-6 col-lg-3 mb-7 mb-lg-0 space-1 cust_centered">
    <!-- Team -->
    <div class="text-center">
    <img class="u-lg-avatar shadow-lg rounded-circle mx-auto mb-4" src="<?php echo get_template_directory_uri(); ?>/assets/img/100x100/img3.jpg" alt="Image Description">
    <h4 class="h5">Shqiponje Pira</h4>
    <span class="d-block text-primary">Secrétaire comptable</span>
    <hr>
    <a class="text-secondary font-size-14" href="#">administration@agepoly</a>
    </div>
    <!-- End Team -->
  </div>
  </div>
</div>
<!-- End Team Section -->

</main>
