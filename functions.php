<?php

/* Menu */

function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
	  'footer-menu' => __('Footer Menu'),
	  'block-menu' => __('Block Menu On Homepage'),
     )
   );
 }
 add_action( 'init', 'register_my_menus' );

 ?>

 <?php
 add_shortcode( 'teamMember', 'teamShortcode' );
 add_shortcode( 'teamMemberAll', 'teamShortcodeAll' ); //name, poste, email, url
 add_shortcode( 'startTeam', 'startTeamSpaceS' );
 add_shortcode( 'endTeam', 'endTeamSpaceS' );
 
 add_shortcode( 'question', 'addQuestionShortcode' ); //question, answer
 add_shortcode( 'startFaq', 'startFaqS' );
 add_shortcode( 'endFaq', 'endFaqS' );
 
 add_shortcode( 'card', 'generateCardShortcode' ); //$name, $desc, $site, $pic
 add_shortcode( 'card_partner', 'generateCardPartnerShortcode' ); //$name, $time, $desc, $how, $site, $pic
 add_shortcode( 'startCardSpace', 'startCardSpaceS' );
 add_shortcode( 'endCardSpace', 'endCardSpaceS' );
 
 function teamShortcode($atts)
 {
	 	 ob_start();

	$atr = shortcode_atts( array(
      'id' => 'undefined',
	  'poste' => false
   ), $atts ); 
   
   generateTeamFromUser($atr['id'], $atr['poste']);
      return ob_get_clean();

 }
 
   function teamShortcodeAll($atts)
 {
	 	 ob_start();

	$atr = shortcode_atts( array(
      'name' => 'undefined',
	  'poste' => 'undefined',
	  'email' => 'undefined',
	  'url' => 'undefined'
   ), $atts ); 
   
   generateTeam($atr['name'], $atr['poste'],  $atr['email'],  $atr['url']);
      return ob_get_clean();

 }
  

  function generateTeamFromUser(string $identifier, $fonction = false)
   {
	   $user = false;
	   $user = get_user_by("login", $identifier);
	   if($user == false)
	   {
		$user = get_user_by("email", $identifier);
	   }
	   	if($user == false)
	   {
		$id = 0;
	   }
	   else
	   {
		   $id = $user->ID;
	   }
	   
	   	generateTeamFromId($id, $fonction);

   }
  
 
  function generateTeamFromId(int $id, $fonction = false)
  
 {
	generateTeam(get_the_author_meta('display_name', $id),  $fonction ? $fonction : get_the_author_meta('nickname', $id),  get_the_author_meta('email', $id), get_avatar_url($id));
 }

 ?>
 <?php

 /* Team */
 function generateTeam($name, $job, $mail, $pic) {?>
   <div class="col-6 col-lg-3 mb-7 mb-lg-0 space-1 cust_centered">
     <!-- Team -->
     <div class="text-center">
     <img class=" u-lg-avatar rounded-circle mx-auto mb-4" src="<?php echo $pic; ?>" alt="Portrait">
     <h4 class="h5"><?php echo $name; ?></h4>
     <span class="d-block text-primary"><?php echo $job; ?></span>
     <hr>
     <a class="text-secondary font-size-14" href="mailto:<?php echo $mail; ?>"><?php echo $mail; ?></a>
     </div>
     <!-- End Team -->
   </div>
   <?php
 }

 function startTeamSpace() {
    echo '<div class="row mb-9 text-center">';
 }

 function endTeamSpace() {
	echo '</div>';
 }

  function startTeamSpaceS() {
    return '<div class="row mb-9 text-center">';
 }

 function endTeamSpaceS() {
	return '</div>';
 }
 /* Cards */

 $cardcpt = 0;
 function generateCardShortcode($atts)
 {
	 ob_start();
		$atr = shortcode_atts( array(
		'name' => 'undefined',
		'desc' => 'undefined',
		'site' => 'undefined',
		'pic' => 'undefined',
		), $atts ); 
   
   generateCard($atr['name'], $atr['desc'], $atr['site'], $atr['pic']); 
   return ob_get_clean();
 }
  function generateCardPartnerShortcode($atts)
 {
	 ob_start();
		$atr = shortcode_atts( array(
		'name' => 'undefined',
		'desc' => 'undefined',
		'time' => 'undefined',
		'how' => 'undefined',
		'site' => 'undefined',
		'pic' => 'undefined',
		), $atts ); 
   
   $desc = "<style>hr {border-top: 1px solid #e3e6f0; margin-right: -2rem; margin-left: -2rem;}</style><hr><div style='height:20px'>".$atr['time']."</div><hr><div style='height:100px'>".$atr['desc']."</div><hr><div style='height:60px'>".$atr['how']."</div>";
   generateCard($atr['name'], $desc, $atr['site'], $atr['pic']); 
   return ob_get_clean();
 }
 function generateCard($name, $desc, $site, $pic) {
   global $cardcpt;

   if($cardcpt == 0) {?>
    <div class="card-deck d-block d-lg-flex card-lg-gutters-2">
     <?php
   }

   ?>
   <div class="card border-0 mb-3">
    <div class="card-body border border-bottom-0 rounded-top text-center p-5">
		<div style="margin: auto;    height: 256px;    width: 256px;">
			<img class="rounded mb-4" style="object-position: center; object-fit: contain; height: 256px; width: 256px;" src="<?php echo $pic; ?>" alt="Icon">
		</div>	
		<h4 class="h5 mb-1"><?php echo $name; ?></h4>
		<p class="mb-0"><?php echo $desc; ?></p>
    </div>

     <div class="card-footer text-center border rounded-bottom p-5">
		<a class="btn btn-sm btn-primary btn-wide" href="<?php echo $site; ?>">Lien</a>
     </div>
   </div>
   <?php

   $cardcpt++;

   if($cardcpt == 3) {
     $cardcpt = 0;
     ?>
    </div>
     <?php
   }
 }

 function startCardSpace() {?>
   <div class="container space-2 text-center">
 <?php
 }

 function endCardSpace() {
   global $cardcpt;
   if($cardcpt != 0)
   {
	      ?></div><?php
   }
   ?></div><?php
   $cardcpt = 0;
 }
 
  function startCardSpaceS() {
   return '<div class="container space-2 text-center">';
 }

 function endCardSpaceS() {
   global $cardcpt;
   if($cardcpt != 0)
   {
	      return '</div></div>';
   }
   	      return '</div>';
   $cardcpt = 0;
 }


 /* FAQ */

 $faqcpt = 0;
 $acccpt = 0;

 function startFaq() {
   global $acccpt; ?>
   <div id="basicsAccordion_<?php echo $acccpt; ?>" class="space-1"> <?php
 }

 function endFaq() {
   global $acccpt, $faqcpt;
   $acccpt++;
   $faqcpt = 0;
   ?> </div> <?php
 }

  function startFaqS() {
	global $acccpt, $faqcpt;
   return '<div id="basicsAccordion_'.$acccpt.'" class="space-1">';
   }

 function endFaqS() {
   global $acccpt, $faqcpt;
   $acccpt++;
   $faqcpt = 0;
   return '</div>';
 } 
 function addQuestionShortcode($atts)
 {
	 	 ob_start();

		$atr = shortcode_atts( array(
		'question' => 'undefined',
		'answer' => 'undefined',
		), $atts ); 
   
   addQuestion($atr['question'], $atr['answer']); 
      return ob_get_clean();

 }
function addQuestion($question, $answer) {
  global $acccpt, $faqcpt; ?>
  <div class="card mb-3">
  <div class="card-header card-collapse__header" id="basicsHeading_<?php echo $acccpt; ?>_<?php echo $faqcpt; ?>">
    <h5 class="mb-0">
    <button class="btn btn-link btn-block d-flex justify-content-between card-collapse__btn <?php if ($faqcpt != 0) echo "collapsed"; ?> p-3"
        data-toggle="collapse"
        data-target="#basicsCollapse_<?php echo $acccpt; ?>_<?php echo $faqcpt; ?>"
        aria-expanded="<?php echo ($faqcpt == 0 ? "true" : "false"); ?>"
        aria-controls="basicsCollapse_<?php echo $acccpt; ?>_<?php echo $faqcpt; ?>">
      <?php echo $question; ?>

      <span class="card-collapse__btn-arrow">
      <span class="fa fa-arrow-down small"></span>
      </span>
    </button>
    </h5>
  </div>
  <div id="basicsCollapse_<?php echo $acccpt; ?>_<?php echo $faqcpt; ?>" class="collapse <?php if ($faqcpt == 0) echo "show"; ?>"
     aria-labelledby="basicsHeading_<?php echo $acccpt; ?>_<?php echo $faqcpt; ?>"
     data-parent="#basicsAccordion_<?php echo $acccpt; ?>">
    <div class="card-body card-collapse__body">
    <?php echo $answer; ?>
    </div>
  </div>
</div> <?php
  $faqcpt++;
}

  ?>
